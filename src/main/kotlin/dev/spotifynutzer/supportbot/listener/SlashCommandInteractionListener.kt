package dev.spotifynutzer.supportbot.listener

import dev.spotifynutzer.supportbot.SupportBot
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class SlashCommandInteractionListener(private val instance: SupportBot) : ListenerAdapter() {

  override fun onSlashCommandInteraction(event: SlashCommandInteractionEvent) {
    instance.commandManager.onCommand(event)
  }
}