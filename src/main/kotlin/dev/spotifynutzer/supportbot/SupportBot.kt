package dev.spotifynutzer.supportbot

import dev.spotifynutzer.supportbot.bot.BotBuilder
import dev.spotifynutzer.supportbot.commands.CommandManager
import dev.spotifynutzer.supportbot.configuration.ConfigProvider
import dev.spotifynutzer.supportbot.database.DatabaseConnector
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.system.exitProcess

class SupportBot {

  val botBuilder: BotBuilder
  private val token: String
  private val mainConfigurationFile: File = File("config.json")
  private val logger: Logger = LoggerFactory.getLogger(SupportBot::class.java)
  val commandManager: CommandManager
  val databaseConnector: DatabaseConnector

  var mainConfiguration: MutableMap<String, Any>
    private set

  init {

    if (!mainConfigurationFile.exists()) {
      mainConfigurationFile.createNewFile()
      logger.error("Configuration file doesn't exist, creating it now...")
      setupConfiguration()
    }

    mainConfiguration = ConfigProvider.getConfigFromFile(mainConfigurationFile) ?: HashMap()

    if (mainConfiguration.isEmpty()) {
      logger.error("Configuration is empty, filling in default values...")
      setupConfiguration()
    }

    token = mainConfiguration["token"] as String

    databaseConnector = DatabaseConnector(this)
    databaseConnector.openConnection()

    botBuilder = BotBuilder(token, this)
    botBuilder.build()

    commandManager = CommandManager(this)
  }

  private fun setupConfiguration() {
    mainConfiguration = HashMap()

    mainConfiguration["token"] = ""
    mainConfiguration["devmode"] = false

    val databaseConfiguration: MutableMap<String, Any> = HashMap()
    databaseConfiguration["ip"] = ""
    databaseConfiguration["port"] = 27017
    databaseConfiguration["user"] = ""
    databaseConfiguration["authDatabase"] = ""
    databaseConfiguration["password"] = ""
    databaseConfiguration["database"] = ""

    mainConfiguration["database"] = databaseConfiguration

    ConfigProvider.saveConfig(mainConfigurationFile, mainConfiguration)
    logger.info("Exiting...")
    exitProcess(1)
  }


}

fun main() {
  SupportBot()
}