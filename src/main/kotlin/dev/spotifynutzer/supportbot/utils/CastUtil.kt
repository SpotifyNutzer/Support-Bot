package dev.spotifynutzer.supportbot.utils

object CastUtil {

  inline fun <reified T> Any?.tryCast(block: T.() -> Unit) {
    if (this is T) {
      block()
    }
  }

  inline fun <reified T> Any?.tryCastOrDefault(default: T): T {
    if (this is T) return this

    return default
  }
}