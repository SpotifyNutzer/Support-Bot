package dev.spotifynutzer.supportbot.utils

import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import java.awt.Color
import java.time.Instant

class EmbedModel(
    private val title: String,
    private val text: String,
    private val guild: Guild,
    private val color: Color = Color.DARK_GRAY,
    private val author: String = guild.name,
    private val authorIcon: String? = guild.iconUrl,
    private val timestamp: Instant = Instant.now()
) {

    private val embedBuilder = EmbedBuilder()

    init {
        embedBuilder.setTitle(title)
        embedBuilder.setDescription(text)
        embedBuilder.setColor(color)
        embedBuilder.setAuthor(author, "https://spotifynutzer.dev", authorIcon)
        embedBuilder.setTimestamp(timestamp)
        embedBuilder.setFooter(guild.name, guild.iconUrl)
    }

    fun addField(title: String, text: String, inline: Boolean = false) {
        embedBuilder.addField(title, text, inline)
    }

    fun reply(event: SlashCommandInteractionEvent) {
        val embed = getEmbed()

        event.replyEmbeds(embed).queue()
    }

    fun getEmbed(): MessageEmbed = embedBuilder.build()

    fun sendMessage(textChannel: TextChannel) {
        val embed = getEmbed()

        textChannel.sendMessageEmbeds(embed).queue()
    }

}