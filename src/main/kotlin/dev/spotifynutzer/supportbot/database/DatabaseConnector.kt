package dev.spotifynutzer.supportbot.database

import com.mongodb.AuthenticationMechanism
import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoDatabase
import dev.spotifynutzer.supportbot.SupportBot
import dev.spotifynutzer.supportbot.utils.CastUtil.tryCastOrDefault
import org.litote.kmongo.KMongo
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress

class DatabaseConnector(instance: SupportBot) {

  private val LOGGER = LoggerFactory.getLogger(DatabaseConnector::class.java)

  private val databaseConfiguration: MutableMap<String, Any> = instance.mainConfiguration["database"].tryCastOrDefault(
    mutableMapOf())

  private val inetSocketAddress: InetSocketAddress = InetSocketAddress(
    databaseConfiguration["ip"].tryCastOrDefault("127.0.0.1"),
    databaseConfiguration["port"].tryCastOrDefault(27017))
  private val user: String = databaseConfiguration["username"].tryCastOrDefault("admin")
  private val authDatabase = databaseConfiguration["authdb"].tryCastOrDefault("admin")
  private val password: String = databaseConfiguration["password"].tryCastOrDefault("password")
  private val database: String = databaseConfiguration["database"].tryCastOrDefault("database")

  lateinit var mongoClient: MongoClient
  lateinit var mongoDatabase: MongoDatabase

  fun openConnection() {
    val mongoCredential =
      MongoCredential.createCredential(user, authDatabase, password.toCharArray()).withMechanism(
        AuthenticationMechanism.SCRAM_SHA_256
      )

    val mongoClientSettings = MongoClientSettings.builder().credential(mongoCredential)
      .applyToClusterSettings { builder ->
        run {
          builder.hosts(listOf(ServerAddress(inetSocketAddress)))
        }
      }.build()

    mongoClient = KMongo.createClient(mongoClientSettings)
    mongoDatabase = mongoClient.getDatabase(database)
  }
}