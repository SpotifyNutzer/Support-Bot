package dev.spotifynutzer.supportbot.bot

import dev.spotifynutzer.supportbot.SupportBot
import dev.spotifynutzer.supportbot.listener.SlashCommandInteractionListener
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.SelfUser
import net.dv8tion.jda.api.requests.GatewayIntent

/**
 * @author SpotifyNutzer
 * Discord: Paul Weber#1234
 * GitHub: https://github.com/SpotifyNutzeer
 */
class BotBuilder(private val token: String, private val instance: SupportBot) {

  private lateinit var jdaBuilder: JDABuilder
  lateinit var jda: JDA
  lateinit var selfUser: SelfUser

  fun build() {
    jdaBuilder = JDABuilder.createDefault(
      token,
      GatewayIntent.GUILD_MEMBERS
    )
      .addEventListeners(
        SlashCommandInteractionListener(instance)
      )

    jdaBuilder.setActivity(Activity.playing("ONLY ONLINE FOR TESTING"))

    jda = jdaBuilder.build()
    selfUser = jda.selfUser

  }


}