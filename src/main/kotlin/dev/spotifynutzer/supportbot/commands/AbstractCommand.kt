package dev.spotifynutzer.supportbot.commands

import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.build.Commands
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData

abstract class AbstractCommand {

  abstract fun onCommand(member: Member, channel: TextChannel, event: SlashCommandInteractionEvent)

  abstract fun getCommand(): String

  abstract fun getDescription(): String

  open fun getCommandData(): SlashCommandData = Commands.slash(getCommand(), getDescription())

}