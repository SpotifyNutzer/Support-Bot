package dev.spotifynutzer.supportbot.commands.impl.administration

import dev.spotifynutzer.supportbot.SupportBot
import dev.spotifynutzer.supportbot.commands.AbstractCommand
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import org.litote.kmongo.eq
import org.litote.kmongo.getCollection

class UpdateGuildsCommand(private val instance: SupportBot) : AbstractCommand() {

  override fun onCommand(member: Member, channel: TextChannel,
    event: SlashCommandInteractionEvent) {

    if (event.user.idLong != 461917424631939092L) {
      event.hook.setEphemeral(true).sendMessage("You are not allowed to run this command!").queue()
      return
    }

    val jda = instance.botBuilder.jda

    event.hook.sendMessage("Update complete!").queue()
  }

  override fun getCommand(): String = "update-guilds"

  override fun getDescription(): String = "ONLY FOR BOT OWNER"
}