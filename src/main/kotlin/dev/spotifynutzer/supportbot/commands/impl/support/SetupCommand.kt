package dev.spotifynutzer.supportbot.commands.impl.support

import dev.spotifynutzer.supportbot.SupportBot
import dev.spotifynutzer.supportbot.commands.AbstractCommand
import dev.spotifynutzer.supportbot.objects.Guild
import dev.spotifynutzer.supportbot.utils.EmbedModel
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.Commands
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData
import net.dv8tion.jda.api.interactions.components.buttons.Button
import org.litote.kmongo.eq
import org.litote.kmongo.getCollection
import org.litote.kmongo.text
import java.awt.Color
import java.time.Instant
import java.util.*
import kotlin.concurrent.schedule

class SetupCommand(private val instance: SupportBot) : AbstractCommand() {

  override fun onCommand(member: Member, channel: TextChannel,
    event: SlashCommandInteractionEvent) {

    if (event.guild == null) {
      event.hook.sendMessage("This command needs to be run in a guild!").queue()
      return
    }

    val guild = event.guild!!
    val name = event.getOption("name")!!.asString
    val role = event.getOption("role")!!.asRole
    val categoryName = event.getOption("category")!!.asString

    if (categoryName.isBlank()) {
      guild.createTextChannel(name)
        .addRolePermissionOverride(role.idLong, setOf(Permission.MANAGE_CHANNEL), null)
        .addRolePermissionOverride(guild.publicRole.idLong, null, setOf(
          Permission.MESSAGE_SEND,
          Permission.MESSAGE_SEND_IN_THREADS,
          Permission.VIEW_CHANNEL
        )).queue()

    } else {

      val category = guild.getCategoriesByName(categoryName, true).first()

      guild.createTextChannel(name, category)
        .addRolePermissionOverride(role.idLong, setOf(Permission.MANAGE_CHANNEL), null)
        .addRolePermissionOverride(guild.publicRole.idLong, null, setOf(
          Permission.MESSAGE_SEND,
          Permission.MESSAGE_SEND_IN_THREADS,
          Permission.VIEW_CHANNEL
        )).queue()
    }


    Timer().schedule(1000L) {
      val textChannel: TextChannel? = guild.getTextChannelsByName(name, false).first()

      if (textChannel == null) {
        event.hook.setEphemeral(true).sendMessage("An error occured!")
        return@schedule
      }

      val embedModel = EmbedModel(
        event.getOption("title")!!.asString,
        event.getOption("text")!!.asString,
        event.guild!!,
        Color.BLUE
      )

      val embed = embedModel.getEmbed()

      textChannel.sendMessageEmbeds(embed).addActionRow(
        Button.primary("create-ticket", "Ticket erstellen")
      ).queue()

      val databaseGuild = instance.databaseConnector.mongoDatabase.getCollection<Guild>().find(Guild::idLong eq guild.idLong).first() ?: Guild(
        guild.idLong,
        textChannel.idLong,
        guild.getCategoriesByName(categoryName, false).first().idLong,
        role.idLong
      )

      databaseGuild.moderationRole = role.idLong
      databaseGuild.ticketChannelIDLong = textChannel.idLong
      databaseGuild.ticketCategoryIDLong = guild.getCategoriesByName(categoryName, false).first().idLong

      databaseGuild.save(instance)


      event.hook.sendMessage("Setup complete!").queue()
    }


  }

  override fun getCommand(): String = "setup"

  override fun getDescription(): String = "Command used for setting up the bot!"

  override fun getCommandData(): SlashCommandData {
    val commandData = Commands.slash(getCommand(), getDescription())
    commandData.addOption(OptionType.STRING, "name", "The name of the Forum Channel", true)
    commandData.addOption(OptionType.ROLE, "role", "The role, which will manage tickets", true)
    commandData.addOption(OptionType.STRING, "category", "The name of the Category", true)
    commandData.addOption(OptionType.STRING, "title", "The title of the Ticket Embed", true)
    commandData.addOption(OptionType.STRING, "text", "The text in the Ticket Embed", true)

    return commandData
  }


}