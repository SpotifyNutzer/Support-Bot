package dev.spotifynutzer.supportbot.commands

import dev.spotifynutzer.supportbot.SupportBot
import dev.spotifynutzer.supportbot.commands.impl.administration.UpdateGuildsCommand
import dev.spotifynutzer.supportbot.commands.impl.support.SetupCommand
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent

class CommandManager(instance: SupportBot) {

  /**     List with all commands  **/
  private val commands: ArrayList<AbstractCommand> = ArrayList()

  init {

    /**     Updating commands   **/
    val commandListUpdateAction = instance.botBuilder.jda.updateCommands()

    commands.add(SetupCommand(instance))
    commands.add(UpdateGuildsCommand(instance))


    commands.forEach {
      commandListUpdateAction.addCommands(it.getCommandData())
    }

    commandListUpdateAction.queue()
  }

  /**     Executing a command     **/
  fun onCommand(event: SlashCommandInteractionEvent) {

    event.deferReply().queue()

    /**     Finding the command     **/
    val command: List<AbstractCommand> = commands.filter {
      it.getCommand() == event.name
    }

    /**     Command couldn´t be found   **/
    if (command.isEmpty()) event.reply(
      "Dieser Befehl konnte nicht gefunden werden. Bitte melde dies SpotifyNutzer | Paul#1234.")
      .queue()

    /**     Executing the command   **/
    command.first().onCommand(event.member!!, event.guildChannel.asTextChannel(), event)

  }

  fun addCommand(vararg command: AbstractCommand) {

    if (!commands.containsAll(command.toList())) commands.addAll(command)

  }

}