package dev.spotifynutzer.supportbot.objects

import dev.spotifynutzer.supportbot.SupportBot
import org.litote.kmongo.getCollection

class Guild(
    val idLong: Long,
    var ticketChannelIDLong: Long,
    var ticketCategoryIDLong: Long,
    var moderationRole: Long

) {

    fun save(instance: SupportBot) {
        instance.databaseConnector.mongoDatabase.getCollection<Guild>().insertOne(this)
    }

}